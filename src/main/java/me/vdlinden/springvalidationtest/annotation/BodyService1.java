package me.vdlinden.springvalidationtest.annotation;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BodyService1 {

    Body1 save(Body1 body) {
        return body;
    }
}
