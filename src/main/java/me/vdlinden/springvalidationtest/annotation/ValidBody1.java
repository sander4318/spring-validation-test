package me.vdlinden.springvalidationtest.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = {BodyValidator1.class})
public @interface ValidBody1 {

    String message() default "Body2 is not valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
