package me.vdlinden.springvalidationtest.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class BodyValidator1 implements ConstraintValidator<ValidBody1, Body1> {
    @Override
    public boolean isValid(Body1 value, ConstraintValidatorContext context) {
        Long one = value.getOne();
        Long higherThanOne = value.getHigherThanOne();
        if (Objects.isNull(one) || Objects.isNull(higherThanOne)) {
            context.buildConstraintViolationWithTemplate("one and higherThanOne may not be null").addConstraintViolation();
            return false;
        }
        if (higherThanOne <= one) {
            context.buildConstraintViolationWithTemplate("higherThanOne must be higher than one").addConstraintViolation();
            return false;
        }
        return true;
    }
}
