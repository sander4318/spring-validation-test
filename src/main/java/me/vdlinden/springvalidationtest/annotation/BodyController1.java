package me.vdlinden.springvalidationtest.annotation;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/annotation")
@RequiredArgsConstructor
class BodyController1 {
    private final BodyService1 service;

    @PostMapping
    Body1 test(@Valid @RequestBody Body1 body) {
        return service.save(body);
    }
}
