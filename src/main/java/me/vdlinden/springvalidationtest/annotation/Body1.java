package me.vdlinden.springvalidationtest.annotation;

import lombok.Data;

import javax.validation.constraints.*;

@Data
@ValidBody1
public class Body1 {
    public static final Long DEFAULT_ONE = 1L;
    public static final Long DEFAULT_HIGHER_THAN_ONE = 2L;

    @Positive
    private Long one = DEFAULT_ONE;

    @Positive
    private Long higherThanOne = DEFAULT_HIGHER_THAN_ONE;
}
