package me.vdlinden.springvalidationtest.animals;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AnimalNotFoundException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Animal with name [%s] is not found.";

    public AnimalNotFoundException(String name) {
        super(String.format(MESSAGE_TEMPLATE, name));
    }

    public AnimalNotFoundException() {
        super(MESSAGE_TEMPLATE);
    }
}
