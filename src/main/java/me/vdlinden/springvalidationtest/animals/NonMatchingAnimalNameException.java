package me.vdlinden.springvalidationtest.animals;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NonMatchingAnimalNameException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Names in url [%s] and body [%s] are not matching";
    public NonMatchingAnimalNameException(String nameInUrl, String nameInBody) {
        super(String.format(MESSAGE_TEMPLATE, nameInUrl, nameInBody));
    }
}
