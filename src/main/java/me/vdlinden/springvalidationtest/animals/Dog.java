package me.vdlinden.springvalidationtest.animals;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
public class Dog implements Animal {
    @NotBlank
    private String name;
    @NotNull
    private Breed breed;
    @NotNull
    @Positive
    @Builder.Default
    private Integer legCount = 4;

    public enum Breed {
        BEAGLE, GERMAN_SHEPARD, HUSKY
    }
}
