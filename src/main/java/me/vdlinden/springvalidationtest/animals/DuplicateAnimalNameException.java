package me.vdlinden.springvalidationtest.animals;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DuplicateAnimalNameException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Animal with name [%s] is not found.";

    public DuplicateAnimalNameException(String message) {
        super(message);
    }

    public DuplicateAnimalNameException(Animal animal) {
        super(String.format(MESSAGE_TEMPLATE, animal.getName()));
    }
}
