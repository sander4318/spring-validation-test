package me.vdlinden.springvalidationtest.animals;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AnimalRepository extends PagingAndSortingRepository<Animal, String> {
}
