package me.vdlinden.springvalidationtest.animals;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@AllArgsConstructor
public class Rat implements Animal {
    @NotBlank
    private String name;
    @NotNull
    @Positive
    private Integer legCount;
}
