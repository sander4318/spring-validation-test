package me.vdlinden.springvalidationtest.animals;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/animals")
@RequiredArgsConstructor
public class AnimalController {
    private final AnimalRepository animalRepository;

    @GetMapping
    public Iterable<Animal> getAnimals() {
        return animalRepository.findAll();
    }

    @GetMapping("/{name}")
    public Animal getAnimal(@PathVariable String name) {
        return animalRepository.findById(name).orElseThrow(() -> new AnimalNotFoundException(name));
    }

    @PostMapping
    public Animal createAnimal(@Valid @RequestBody Animal animal) {
        return animalRepository.save(animal);
    }

    @PutMapping("/{name}")
    public Animal updateAnimal(@PathVariable String name, @Valid @RequestBody Animal animal) {
        if (!name.equalsIgnoreCase(animal.getName())) throw new NonMatchingAnimalNameException(name, animal.getName());
        return animalRepository.save(animal);
    }

    @DeleteMapping("/{name}")
    public void deleteAnimal(@PathVariable String name) {
        animalRepository.deleteById(name);
    }
}
