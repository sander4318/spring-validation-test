package me.vdlinden.springvalidationtest.validator;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/validator")
@RequiredArgsConstructor
class BodyController2 {
    private final BodyService2 service;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new BodyValidator2());
    }

    @PostMapping
    Body2 test(@Valid @RequestBody Body2 body) {
        return service.save(body);
    }
}
