package me.vdlinden.springvalidationtest.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

public class BodyValidator2 implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Body2.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Body2 body = (Body2) o;
        Long one = body.getOne();
        Long higherThanOne = body.getHigherThanOne();
        if (Objects.isNull(one) || Objects.isNull(higherThanOne)) {
            errors.reject("body.invalid", "one and higherThanOne may not be null");
            return;
        }
        if (higherThanOne <= one) {
            errors.reject("body.invalid", "higherThanOne must be higher than one");
        }
    }
}
