package me.vdlinden.springvalidationtest;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class SpringValidationTestApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @ParameterizedTest(name = "{0}")
    @ArgumentsSource(BodyArgumentsProvider.class)
    void annotation(@SuppressWarnings("unused") String testName, String content, Integer expectedStatus) throws Exception {
        mockMvc.perform(post("/api/annotation").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is(expectedStatus));
    }

    @ParameterizedTest(name = "{0}")
    @ArgumentsSource(BodyArgumentsProvider.class)
    void validator(@SuppressWarnings("unused") String testName, String content, Integer expectedStatus) throws Exception {
        mockMvc.perform(post("/api/validator").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(status().is(expectedStatus));
    }

    private static final class BodyArgumentsProvider implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) {
            return Stream.of(
                    // String testName, String content, Integer expectedStatus
                    new Object[]{"Empty string", "", 400},
                    new Object[]{"Empty array", "[]", 400},
                    new Object[]{"Empty object", "{}", 200},
                    new Object[]{"One is equal", fileToString("bodyOneEqual.json"), 400},
                    new Object[]{"One is negative", fileToString("bodyOneNegative.json"), 400},
                    new Object[]{"One is missing", fileToString("bodyOneMissing.json"), 200},
                    new Object[]{"One is null", fileToString("bodyOneNull.json"), 400},
                    new Object[]{"All values supplied", fileToString("bodyValid.json"), 200}
            ).map(Arguments::of);
        }
    }

    private static String fileToString(String filename) {
        try (InputStream input = new ClassPathResource(filename).getInputStream()) {
            return IOUtils.toString(input, Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
